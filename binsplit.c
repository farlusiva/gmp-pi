/* This is an idea I came up with
 * It's like binary splitting but *slower* than the normal version
 *
 * Here's what it does:
 * Say we have a fraction, s1/s2.
 * (At the start, this is the first (zeroth, k=0) round of the summing
 *
 * What we do in our summing is we calculate our thing, a fraction a/b.
 *
 * Then we use fractionmagic to add s1/s2 and a/b without doing any division.
 *
 * (This becomes (s2*a + s1*b) / (b * s2))
 *
 * Then we simplify our new fraction, and call it s1/s2.
 *
 * This process continues.
 *
 * And instead of actually calculating the beginning part,
 * we do fraction multiplication with it so that we can
 * avoid the division.
 *
 * Now we have a fraction t1/t2.
 *
 * This is basically the value of the Chudnovsky formula.
 * Because that is 1/pi, t2/t1 is pi.
 * (This means we don't have to calc 1/(t1/t2), which is two full-prec divisions)
 *
 * So, in short, this will only do one full-precision division.
 *
 * All stuff is integer (mpz) arithmetic, with the exception of the last
 * part because the beginning fraction has a square root.
 *
 *
 *
 * For some unknown reason, this is slower.
 * Probably because it isn't the standard "lots-of-wierd-recursive-functions-stuff"
 * binary splitting.
 *
 * This could be called binary splitting by the fact that it does reduce
 * the entire formula to a fraction, meaning that we only need one full-prec
 * division at the end.
 *
 */

#define _POSIX_C_SOURCE 200809L /* Use POSIX.1-2008 */

#include <errno.h> /* args.c */
#include <gmp.h> /* mpw_pow_ui et al. */
#include <stdio.h> /* printf */
#include <stdlib.h> /* args.c */
#include <getopt.h> /* args.c */



/* Practical macros */
#define A 13591409
#define B 545140134
#define C 640320
#define D 10005
#define E 426880

static double bits_per_digit = 3.3219280948873626; /* log2(10) */

/* Pi formula: craig-wood.com/nick/images/math/40f2e34873e611fb04c7b93ed846b724.png
 * (The bottom one) */



int main(int argc, char* argv[]) {

	/* Parse args */
	int digits = 10000;
	int statusbar = 1;
	#include "args.c"

	int precision = digits * bits_per_digit + 10; /* +10 incase of roundoff */

	mpf_set_default_prec(precision);


	int rounds = (digits / 14) + 1; /* Converges at about 7 digits per round */



	/* Define loads of temp variables */
	mpz_t upper1, upper2, upper3, lower1, lower2, lower3, k;

	mpz_init(k);
	mpz_init(upper1);
	mpz_init(upper2);
	mpz_init(upper3);
	mpz_init(lower1);
	mpz_init(lower2);
	mpz_init(lower3);



	/* Mpz for -1 */
	mpz_t negone;
	mpz_init(negone);
	mpz_sub_ui(negone, negone, 1);
	/* Can't add -1, but can subtract 1???? */



	/* Numerator and denominator for the_sum, basically */
	mpz_t s1, s2;
	mpz_init(s1);
	mpz_init(s2);

	/* Precalculate first round (s1 just happends to be A) */
	mpz_set_ui(s1, A);
	mpz_set_ui(s2, 1);


	/* Temp values of s1 and s2 to be used until r1 and r2 can be */
	/* safely set to s1 and s2 */
	mpz_t r1, r2;
	mpz_init(r1);
	mpz_init(r2);


	/* Temp mpz */
	mpz_t tempz;
	mpz_init(tempz);

	/* Temp mpf */
	mpf_t tempf;
	mpf_init(tempf);


	/* Gcd mpz used for simplifying s1 / s2 */
	mpz_t the_gcd;
	mpz_init(the_gcd);



	/* Now that i think about it, that is probably because -1 is C
	 * taking an unsigned int and reducing one, or something. */


	if (statusbar) printf("\n"); /* sacrifice for status bar */

	for (int a = 1; a < rounds; a++) {

		mpz_set_ui(k, a);


		if (a % 100 == 0 && statusbar) {

			/* Delete previos statusbar line
			 * The escape codes basically tell the terminal to move the cursor
			 * up a line, and then delete the line. */
			printf("\033[F\033[K");

			printf("%i, %i\n", a, rounds);
		}


		/* upper1 = (-1) ** k
		 * upper2 = (6k)!
		 * upper3 = 13591409 + 545140134k */

		mpz_pow_ui(upper1, negone, a);
		mpz_fac_ui(upper2, 6*a);
		mpz_mul_ui(upper3, k, B);
		mpz_add_ui(upper3, upper3, A);

		/* upper1 = upper1 * upper2 * upper3 */
		mpz_mul(upper2, upper2, upper3);
		mpz_mul(upper1, upper1, upper2);


		/* Begin on the lower */

		/* lower1 = (3k)!
		 * lower2 = (k!) ** 3
		 * lower3 = 640320 ** (3k) */

		mpz_fac_ui(lower1, 3*a);
		mpz_fac_ui(lower2, a);
		mpz_pow_ui(lower2, lower2, 3);
		mpz_ui_pow_ui(lower3, C, 3*a);


		/* Multiply together the lower's */

		/* lower1 = lower1 * lower2 * lower3 */
		mpz_mul(lower2, lower2, lower3);
		mpz_mul(lower1, lower1, lower2);


		/* At this point, upper1 is the upper part of the fraction and
		 * lower1 is the lower part of the fraction
		 */

		/* Assuming a = upper1, b = lower1:*/

			/* r1 = (a * s1) + (b * s1) */


		/* r1 = (a * s2) + (b * s1) */
		mpz_mul(r1, upper1, s2);
		mpz_mul(tempz, lower1, s1);
		mpz_add(r1, r1, tempz);


		/* r2 = b * s2 */
		mpz_mul(r2, lower1, s2);

		/* s1, s2 = r1, r2 */
		mpz_set(s1, r1);
		mpz_set(s2, r2);


		/* Simplify the fraction */
		mpz_gcd(the_gcd, s1, s2);

		/* Divide by the gcd to fully simplify fraction */
		mpz_div(s1, s1, the_gcd);
		mpz_div(s2, s2, the_gcd);

	}


	/* Now that we have the fraction s1/s2,
	* we use fraction stuff to multiply it with
	* what used to be the var "beginning" */


	/* s1 and s2 but mpf's */
	mpf_t t1, t2;
	mpf_init(t1);
	mpf_init(t2);

	mpf_set_z(t1, s1);
	mpf_set_z(t2, s2);



	/* At this point, we multiply our fraction with the fraction that is
	 * outside of the sum. */

	/* This fraction is 1 / (426880 * sqrt(10005)) */

	/* Because the numerator is one, we only calculate the denominator
	 * Then we do fraction multiplication with t1/t2. */



	/* tempf = 426880 * sqrt(10005) */
	mpf_set_ui(tempf, D);
	mpf_sqrt(tempf, tempf);
	mpf_mul_ui(tempf, tempf, E);


	/* Do the fraction multiplication, but we don't need to multiply t1 with
	   anything, so we only multiply t2. */

	mpf_mul(t2, t2, tempf);


	/* At this point, t1/t2 is the 1/pi fraction.
	 *
	 * Therefore, t2/t1 is pi.
	 */


	mpf_t pi;
	mpf_init(pi);

	/* If s1/s2 = 1/pi, then s2/s1 = pi */
	mpf_div(pi, t2, t1);


	/* Print the fraction we've generated */
	/*
	mpf_out_str(stdout, 10, digits, t2);
	printf("\n");

	mpf_out_str(stdout, 10, digits, t1);
	printf("\n");
	*/



	/* We have to make a long int for the exponent for some reason
	 * Also we have to make a variable for it too. */
	long int exponent = 1;

	/* All digits of "pi", including a few extra added by GMP */
	char* tmppi = mpf_get_str(NULL, &exponent, 10, 0, pi);
	tmppi[digits+1] = '\0'; /* Truncation rounding */

	/* Insert a decimal point at the appropriate place */

	char pistr[digits+2]; /* +3 for the "3." and the null terminator */
	tmppi[0] = '.';
	sprintf(pistr, "3%s", tmppi);

	printf("%s\n", pistr);



	/* Clear all the GMP stuff so we're left only with the pi string */
	mpz_clear(upper1);
	mpz_clear(upper2);
	mpz_clear(upper3);
	mpz_clear(lower1);
	mpz_clear(lower2);
	mpz_clear(lower3);
	mpz_clear(negone);
	mpz_clear(s1);
	mpz_clear(s2);
	mpz_clear(r1);
	mpz_clear(r2);
	mpf_clear(t1);
	mpf_clear(t2);
	mpz_clear(tempz);
	mpf_clear(tempf);
	mpf_clear(pi);

	return 0;
}

