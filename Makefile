CC=cc
CFLAGS=-lgmp -O3 -march=native -pipe -Wall -std=c99 -pedantic


all: chudnovsky

chudnovsky ramanujan binsplit:
	$(CC) $@.c $(CFLAGS) -o $@

clean:
	rm -f a.out binsplit chudnovsky ramanujan
