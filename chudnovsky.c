/* chudnovsky.c
 *
 * This should be the fastest one.
 */


#define _POSIX_C_SOURCE 200809L /* Use POSIX.1-2008 */

#include <errno.h> /* args.c */
#include <gmp.h> /* mpf_pow_ui et al. */
#include <stdio.h> /* printf */
#include <stdlib.h> /* args.c */
#include <getopt.h> /* args.c */


/* Practical macros */
#define A 13591409
#define B 545140134
#define C 640320

static double bits_per_digit = 3.3219280948873626; /* log2(10) */



/* Pi formula: craig-wood.com/nick/images/math/40f2e34873e611fb04c7b93ed846b724.png
 * (The middle one)
 */




int main(int argc, char* argv[]) {


	/* Parse args */
	int digits = 10000;
	int statusbar = 1;
	#include "args.c"

	int precision = digits * bits_per_digit + 10; /* +10 incase of roundoff */
	mpf_set_default_prec(precision);

	int rounds = (digits / 14) + 1; /* Converges at about 7 digits per round */


	/* Number to multiply our sum with */
	mpf_t beginning;
	mpf_init(beginning);


	/* Loads of temporary variables */
	mpz_t upper1, upper2, upper3, lower1, lower2, lower3, k;

	mpz_init(k);
	mpz_init(upper1);
	mpz_init(upper2);
	mpz_init(upper3);
	mpz_init(lower1);
	mpz_init(lower2);
	mpz_init(lower3);


	/* The sum */
	mpf_t sum;
	mpf_init(sum);


	/* Result of upper / lower */
	mpf_t div;
	mpf_init(div);


	/* Upper and lower, the MPF (float) versions */

	mpf_t upper, lower;

	mpf_init(upper);
	mpf_init(lower);


	/* mpz for -1 */
	mpz_t negone;
	mpz_init(negone);
	mpz_sub_ui(negone, negone, 1);
	/* Can't add -1, but can subtract 1???? */

	/* Now that i think about it, that is probably because -1 is C
	 * taking an unsigned int and reducing one, or something. */


	/* Here begins the calculating */

	/* beginning = 12/(640320*sqrt(640320)) */

	mpf_set_ui(beginning, C);
	mpf_sqrt(beginning, beginning);
	mpf_mul_ui(beginning, beginning, C);
	mpf_ui_div(beginning, 12, beginning);

	/*
	mpf_out_str(stdout, 10, digits, beginning); return 0;
	*/


	if (statusbar) printf("\n"); /* sacrifice for status bar */


	for (int a = 0; a < rounds; a++) {

		mpz_set_ui(k, a);

		if (a % 100 == 0 && statusbar) {

			/* Delete previos statusbar line
			 * The escape codes basically tell the terminal to move the cursor
			 * up a line, and then delete the line. */
			printf("\033[F\033[K");

			printf("%i, %i\n", a, rounds);
		}



		/* upper1 = (-1) ** k
		 * upper2 = (6k)!
		 * upper3 = 13591409 + 545140134k */
		mpz_pow_ui(upper1, negone, a);
		mpz_fac_ui(upper2, 6*a);
		mpz_set_ui(upper3, A);
		mpz_addmul_ui(upper3, k, B);


		/* Multiply the upper's */
		mpz_mul(upper2, upper2, upper3);
		mpz_mul(upper1, upper1, upper2);


		/* lower1 = (3k)! */
		/* lower2 = k!**3 */
		/* lower3 = 640320 ** (3k) */

		mpz_fac_ui(lower1,3*a);
		mpz_fac_ui(lower2, a);
		mpz_pow_ui(lower2, lower2, 3);
		mpz_ui_pow_ui(lower3, C, 3*a);

		/* Multiply the lowers together */
		mpz_mul(lower2, lower2, lower3);
		mpz_mul(lower1, lower1, lower2);


		/* Convert upper1/lower1 to mpf's */
		mpf_set_z(upper, upper1);
		mpf_set_z(lower, lower1);


		/* add upper/lower to our sum */
		mpf_div(div, upper, lower);
		mpf_add(sum, sum, div);


	}



	/* We're getting closer... */
	mpf_t pi;
	mpf_init(pi);

	/* At this point we have 1/pi */
	mpf_mul(pi, sum, beginning);

	/* "ui" before div = first thing dividing is NOT mpz */
	mpf_ui_div(pi, 1, pi);



	/* We have to make a long int for the exponent for some reason
	 * Also we have to make a variable for it too. */
	long int exponent = 1;

	/* All digits of "pi", including a few extra added by GMP */
	char* tmppi = mpf_get_str(NULL, &exponent, 10, 0, pi);
	tmppi[digits+1] = '\0'; /* Truncation rounding */

	/* Insert a decimal point at the appropriate place */

	char pistr[digits+2]; /* +3 for the "3." and the null terminator */
	tmppi[0] = '.'; /* also gets rid of the '3' at the beginning */
	sprintf(pistr, "3%s", tmppi);

	printf("%s\n", pistr);


	/* Clear all the GMP stuff so we're left only with the pi string */
	mpf_clear(upper);
	mpz_clear(upper1);
	mpz_clear(upper2);
	mpz_clear(upper3);
	mpf_clear(lower);
	mpz_clear(lower1);
	mpz_clear(lower2);
	mpz_clear(lower3);
	mpz_clear(negone);
	mpf_clear(sum);
	mpf_clear(div);
	mpf_clear(beginning);
	mpf_clear(pi);

	return 0;
}

