/* args.c
 *
 * To be included *inside* the main function, parsing args in a way that's
 *  intended to reduce code reuse, as the programs are virtually identical
 *  except for the method to calculate pi.
 *
 * Expects main's args to be usual two-argument argc and argv.
 * errno.h, getopt.h and stdlib.h must also be included.
 *
 * The variables that are used here, namely (both ints) 'digits' and 'statusbar'
 *  must be declared in the program including this, before the inclusion.
 *
 */

int opt;

while ((opt = getopt(argc, argv, "d:hs")) != -1)

	switch (opt) {


	case 'd':
	digits = strtol(optarg, NULL, 10); /* discard the string part */

	if (errno == ERANGE) {
		fprintf(stderr, "The value provided for -d is out of range.\n");
		return 1;
	}

	if (digits < 1) {
		fprintf(stderr, "The value for -d must be a positive nonzero integer.\n");
		return 1;
	}

	break;

	case '?': /* fallthrough on errors */
	case 'h':
	printf("usage: %s [ARGS]\n"
		   "Arguments:\n"
		   "  -d        tell the program how many digits to use (default 10k)\n"
		   "  -h        display this message\n"
		   "  -s        don't show the status bar\n",
		   argv[0]);
	return 0;

	case 's':
	statusbar = 0;
	break;

	}

