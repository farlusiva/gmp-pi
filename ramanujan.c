/* ramanujan.c
 *
 * This one uses a much simpler formula, although it's also slower than
 *  chudnovsky.
 *
 *
 */

#define _POSIX_C_SOURCE 200809L /* Use POSIX.1-2008 */


#include <errno.h> /* args.c */
#include <gmp.h> /* mpf_pow_ui et al. */
#include <stdio.h> /* printf */
#include <stdlib.h> /* args.c */
#include <getopt.h> /* args.c */


double bits_per_digit = 3.3219280948873626; /* log2(10) */


/* Pi formula:
 * wikimedia.org/api/rest_v1/media/math/render/svg/911bfd1b5a6584fdcd7f82c6a92cc67c088e5659
 */



int main(int argc, char* argv[]) {

	/* Parse args */
	int digits = 10000;
	int statusbar = 1;
	#include "args.c"

	int precision = digits * bits_per_digit + 10; /* +10 incase of roundoff */
	mpf_set_default_prec(precision);

	int rounds = (digits / 7) + 1; /* Converges at about 7 digits per round */


	mpf_t beginning, temp;
	mpf_init(beginning);
	mpf_init(temp);

	mpf_set_str(temp, "9801", 10);


	mpf_set_str(beginning, "8", 10); /* Set beginning to "8" (in base 10) */
	mpf_sqrt(beginning, beginning); /* Set beginning to sqrt(beginning) */


	mpf_div(beginning, beginning, temp);



	mpf_clear(temp); /* We are now done with "temp". This frees it's ram space */


	/* Define loads of temp variables */
	mpz_t upper1, upper2, lower1, lower2, k;

	mpz_init(k);
	mpz_init(upper1);
	mpz_init(upper2);
	mpz_init(lower1);
	mpz_init(lower2);

	double fourk;


	mpf_t sum;
	mpf_init(sum);

	/* Result of upper / lower */
	mpf_t div;
	mpf_init(div);


	/* Upper and lower, the MPF (float) versions */

	mpf_t upper, lower;
	mpf_init(upper);
	mpf_init(lower);




	if (statusbar) printf("\n"); /* sacrifice for status bar */

	for (int a = 0; a < rounds; a++) {


		if (a % 100 == 0 && statusbar) {

			/* Delete previos statusbar line
			 * The escape codes basically tell the terminal to move the cursor
			 * up a line, and then delete the line. */
			printf("\033[F\033[K");

		   printf("%i, %i\n", a, rounds);
		}


		fourk = 4*a;

		mpz_set_d(k, a);
		/* mpz version of a */

		/* upper1 = (4k)!
		 * upper2 = 26390*k + 1103 */
		mpz_fac_ui(upper1, fourk);
		mpz_set_ui(upper2, 1103);
		mpz_addmul_ui(upper2, k, 26390);

		mpz_mul(upper1, upper1, upper2);
		/* Multiply the upper's */


		/* lower1 = (k!) ** 4
		 * lower2 = 396 ** (4k) */
		mpz_fac_ui(lower1, a);
		mpz_pow_ui(lower1, lower1, 4);
		mpz_ui_pow_ui(lower2, 396, fourk);


		mpz_mul(lower1, lower1, lower2);
		/* Multiply the lower's */


		/* Convery upper1/lower1 to mpf's */
		mpf_set_z(upper, upper1);
		mpf_set_z(lower, lower1);

		mpf_div(div, upper, lower);

		mpf_add(sum, sum, div);


	}



	/* We're getting closer... */
	mpf_t pi;
	mpf_init(pi);

	/* At this point we have 1/pi */
	mpf_mul(pi, sum, beginning);

	/* "ui" before div = first thing dividing is NOT mpz */
	mpf_ui_div(pi, 1, pi);


	/* We have to make a long int for the exponent for some reason
	 * Also we have to make a variable for it too. */
	long int exponent = 1;

	/* All digits of "pi", including a few extra added by GMP */
	char* tmppi = mpf_get_str(NULL, &exponent, 10, 0, pi);
	tmppi[digits+1] = '\0'; /* Truncation rounding */

	/* Insert a decimal point at the appropriate place */

	char pistr[digits+2]; /* +3 for the "3." and the null terminator */
	tmppi[0] = '.';
	sprintf(pistr, "3%s", tmppi);

	printf("%s\n", pistr);



	/* Clear all the GMP stuff so we're left only with the pi string */
	mpf_clear(upper);
	mpz_clear(upper1);
	mpz_clear(upper2);
	mpf_clear(lower);
	mpz_clear(lower1);
	mpz_clear(lower2);
	mpf_clear(sum);
	mpf_clear(div);
	mpf_clear(beginning);
	mpf_clear(pi);

	return 0;
}

