# gmp-pi

## About

If you're looking for a C beginner's attempt to calculate pi with GMP, you've
come to the right place.


## Requirements

GCC, GMP (should come with gcc as it's a dependency), some sort of make program,
some sort of shell to run it all, the dependencies of everything mentioned and
the dependencies of those etc.
