# vim: syn=conf

{ pkgs ? import <nixpkgs> {} }:

  pkgs.mkShell {
    buildInputs = with pkgs; [
      mpfr
    ];
}
